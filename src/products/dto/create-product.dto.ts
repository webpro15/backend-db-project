import { IsNotEmpty, Length } from 'class-validator';

export class CreateProductDto {
  @IsNotEmpty()
  id: number;

  @IsNotEmpty()
  @Length(4, 16)
  name: string;

  @IsNotEmpty()
  price: number;
}
